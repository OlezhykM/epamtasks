﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            Polynomial p1 = new Polynomial(1, 2);           //creation first instance
            Polynomial p2 = new Polynomial(10, 20, 30, 40); //creation second instance

            Console.WriteLine(p1 + p2);                     //addition polynomials
            Console.WriteLine(p1 - p2);                     //subtraction polynomials
            Console.WriteLine(p1 * p2);                     //multiplying polynomials
            Console.WriteLine((p1 * p2).Calculate(1.2d));   //calculating polynomials

            Console.ReadLine();
        }
    }
}

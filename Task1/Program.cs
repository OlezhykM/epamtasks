﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] firstArray = { 0, 9, 8, 7, 6, 5, 4, 3, 2, 1 };    //creation of the first array
            int[] secondArray = { 1, 2, 3, 4, 6, 5, 7, 8, 9, 0 };   //creation of the second array

            MyArrays myArr1 = new MyArrays();                       //instance with empty constructor
            MyArrays myArr2 = new MyArrays(firstArray);             //instance that takes one array
            MyArrays myArr3 = new MyArrays(firstArray, 3, 6);       //instance that takes one array and intervals

            myArr2.GetElementByIndex(4);                            //getting element by index
            Console.WriteLine();

            myArr3.GetInterval();                                   //getting intervals
            Console.WriteLine("\n");

            myArr3.AddArrys(firstArray, secondArray);               //addition of arrays
            Console.WriteLine();

            myArr3.SubArrays(firstArray, secondArray);              //subtraction of arrays
            Console.WriteLine();

            myArr2.MultScalar(4);                                   //multiplication by a scalar
            Console.WriteLine();

            myArr2.CompareArrays(firstArray, secondArray);          //comparing of arrays
            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class MyArrays
    {
        #region Fields
        private int[] _Arr;
        private int _beginsInterval, _endsInterval;
        #endregion

        #region Constructors
        public MyArrays()
        {

        }
        public MyArrays(int[] Arr)
        {
            this._Arr = Arr;
        }

        public MyArrays(int[] Arr, int beginsInterval, int endsInterval)
        {
            this._Arr = Arr;
            this._beginsInterval = beginsInterval;
            this._endsInterval = endsInterval;
        }

        #endregion

        #region Methods
        //getting element by index
        public void GetElementByIndex(int index)
        {
            if (index < 0 || index > _Arr.Length)
            {
                throw new IndexOutOfRangeException();
            }
            Console.WriteLine($"{index}-th element of array is {_Arr[index]}");
        }

        //getting intervals
        public void GetInterval()
        {
            if ((_beginsInterval < 0 || _beginsInterval > _endsInterval) || (_endsInterval > _Arr.Length || _endsInterval < _beginsInterval))
            {
                throw new IndexOutOfRangeException();
            }
            Console.WriteLine("interval elements of the array");
            for (int i = _beginsInterval; i <= _endsInterval; i++)
            {
                Console.Write($"{_Arr[i]}");
            }
        }

        //addition of arrays
        public void AddArrys(int[] x, int[] y)
        {
            int result;
            for (int i = _beginsInterval; i < _endsInterval; i++)
            {
                result = x[i] + y[i];
                Console.WriteLine($"{x[i]} + {y[i]} = {result}");
            }
        }

        //subtraction of arrays
        public void SubArrays(int[] x, int[] y)
        {
            int result;
            for (int i = _beginsInterval; i < _endsInterval; i++)
            {
                result = x[i] - y[i];
                Console.WriteLine($"{x[i]} - {y[i]} = {result}");
            }
        }

        //multiplication by a scalar
        public void MultScalar(int number)
        {
            int result;
            for (int i = 0; i < _Arr.Length; i++)
            {
                result = _Arr[i] * number;
                Console.WriteLine($"{_Arr[i]} * {number} = {result}");
            }
        }

        //comparing of arrays
        public void CompareArrays(int[] x, int[] y)
        {
            for (int i = 0; i < _Arr.Length; i++)
            {
                if (x[i] == y[i])
                    Console.WriteLine($"{i} elements of arrays are equal");                
            }
        }

        #endregion
    }
}

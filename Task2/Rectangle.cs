﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class Rectangles
    {
        #region Fields & Constructors
        private int _axisX, _axisY, _width, _height;
        public Rectangles()
        {

        }
        public Rectangles(int axisX, int axisY, int width, int height)
        {
            this._axisX = axisX;
            this._axisY = axisY;
            this._width = width;
            this._height = height;
        }
        #endregion

        #region Methods
        //moving rectangle
        public void Move(int moveX, int moveY)
        {
            _axisX += moveX;
            _axisY += moveY;
        }

        //resize
        public void ChangeSize(int width, int height)
        {
            _width += width;
            _height += height;
        }

        //Show rectangle
        public void Show()
        {
            Console.WriteLine($"Rectangle with axis x: {_axisX}, axis y: {_axisY} Width:{_width}, Height {_height}");
        }

        //rectangle, that containing two rectangles
        public void ContainsTwo(Rectangles rec1, Rectangles rec2)
        {
            int thirdRectangleWidth, thirdRectangleHeight;
            int firstLength = (rec1._axisX + rec1._width) + (rec1._axisY + rec1._height);
            int secondLength = (rec2._axisX + rec2._width) + (rec2._axisY + rec2._height);
            if (firstLength > secondLength)
            {
                thirdRectangleWidth = rec1._axisX + rec1._width;
                thirdRectangleHeight = rec1._axisY + rec1._height;
            }
            else
            {
                thirdRectangleWidth = rec2._axisX + rec2._width;
                thirdRectangleHeight = rec2._axisY + rec2._height;
            }
            Console.WriteLine($"Width: {thirdRectangleWidth}, Height:{thirdRectangleHeight}");
        }

        //intersection of two rectangles
        public void Intersection(Rectangles rec1, Rectangles rec2)
        {

            int x1 = Math.Max(rec1._axisX, rec2._axisX);
            int x2 = Math.Min(rec1._axisX + rec1._width, rec2._axisX + rec2._width);
            int y1 = Math.Max(rec1._axisY, rec2._axisY);
            int y2 = Math.Min(rec1._axisY + rec1._height, rec2._axisY + rec2._height);

            if (x2 >= x1 && y2 >= y1)
            {
                Console.WriteLine($"{ x1}, {y1}, {x2 - x1}, {y2 - y1}");
            }
            else
            {
                Console.WriteLine("Rectangles do not intersect");
            }
        }
        #endregion
    }
}
